﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndLevel : MonoBehaviour, IObservable
{
    [SerializeField] private GameObject _deathMenu;
    [SerializeField] private GameObject _congratulation;
    [SerializeField] private Animator _victoryAnimator;
    public void PlayerDied()
    {
        _deathMenu.SetActive(true);
    }

    public void PlayerPassed(KittenType color)
    {
        _congratulation.SetActive(true);
        _victoryAnimator.SetTrigger(color.GetDescription());
    }

    // Start is called before the first frame update
    private void Awake()
    {
        Observer.AddObservable(this);
    }

    private void OnDestroy()
    {
        Observer.RemoveObservable(this);
    }
    
    public void RestartLevel()
    {
        Level.RestartScene();
    }
}
