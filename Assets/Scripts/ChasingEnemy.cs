﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChasingEnemy : Enemy
{
    private Kitten _target;
    protected override void OnTriggerEnter2D(Collider2D collision)
    {
        base.OnTriggerEnter2D(collision);
        if (collision.GetComponent<Kitten>() != null)
        { 
            _target = collision.GetComponent<Kitten>();
        }
    }

    protected override void FixedUpdate()
    {
        if (_isAgressive)
        {
            _rigidbody2D.MovePosition(_rigidbody2D.position + Vector2.ClampMagnitude((Vector2)_target.transform.position - _rigidbody2D.position, 1) * _speed * Time.fixedDeltaTime);
            if (_rigidbody2D.position.x > _target.transform.position.x)
            { 
                transform.localScale = new Vector3(-1, 1);
            }
            else
            {
                transform.localScale = new Vector3(1, 1);
            }
        }
    }
}
