﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomKitten : MonoBehaviour
{   
    [SerializeField] List<Kitten> _kittens;
    private Kitten _currentKitten;
    public KittenType CurrentKittenType => _currentKitten.KittenType;

    void Start()
    {
        _currentKitten = Instantiate(_kittens[Random.Range(0, _kittens.Count)], transform.position, Quaternion.identity, transform);
    }
    
}
