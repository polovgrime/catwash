﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Score : MonoBehaviour, IObservable
{
    TextMeshProUGUI tmp;

    public void PlayerDied()
    {
        PlayerPrefs.SetInt("score", 0);
        RefreshScore();
    }

    public void PlayerPassed(KittenType color)
    {
        var currentScore = GetScore();
        currentScore += 100;
        PlayerPrefs.SetInt("score", currentScore);
        var highestScore = PlayerPrefs.GetInt("highest" + SceneManager.GetActiveScene().name);
        if (currentScore > highestScore)
            PlayerPrefs.SetInt("highest" + SceneManager.GetActiveScene().name, currentScore);
        RefreshScore();
    }

    public static int GetScore()
    {
        return PlayerPrefs.GetInt("score");
    }

    // Start is called before the first frame update
    void Start()
    {
        tmp = GetComponent<TextMeshProUGUI>();
        RefreshScore();
    }

    private void RefreshScore()
    {
        tmp.text = GetScore().ToString();
    }

    private void Awake()
    {
        Observer.AddObservable(this);
    }

    private void OnDestroy()
    {
        Observer.RemoveObservable(this);
    }
}
