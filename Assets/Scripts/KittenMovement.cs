﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KittenMovement : MonoBehaviour, IObservable
{
    [SerializeField] private float _moveSpeed = 5f;
    [SerializeField] private Joystick _joystick;
    private Rigidbody2D _rigidbody;
    private Vector2 movementDirection;
    private bool isMoving = true;
    // Start is called before the first frame update
    private void Awake()
    {
        Observer.AddObservable(this);
        _rigidbody = GetComponent<Rigidbody2D>();
    }

    private void Update()
    {
        if (false)
            movementDirection = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
        else
            movementDirection = new Vector2(_joystick.Horizontal, _joystick.Vertical);

    }

    // Update is called once per frame
    private void FixedUpdate()
    {
        if (isMoving) _rigidbody.MovePosition(_rigidbody.position + movementDirection * _moveSpeed * Time.fixedDeltaTime);
    }

    private void OnDestroy()
    {
        Observer.RemoveObservable(this);
    }

    public void PlayerDied()
    {
        StopMovement();
    }

    public void PlayerPassed(KittenType color)
    {
        StopMovement();
    }

    private void StopMovement()
    {
        isMoving = false;
    }
}
