﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Music : MonoBehaviour, IObservable
{
    [SerializeField] AudioClip _menuTheme;
    [SerializeField] AudioClip _mainTheme;
    [SerializeField] AudioClip _death;
    [SerializeField] AudioClip _pass;
    private AudioSource _audio;
    private void Start()
    {
        _audio = GetComponent<AudioSource>();
        PlayMenuTheme();
        DontDestroyOnLoad(gameObject);
    }

    public void PlayMainTheme()
    {
        _audio.loop = true;
        _audio.clip = _mainTheme;
        _audio.Play();
    }

    private void PlayMenuTheme()
    {
        _audio.loop = true;
        _audio.clip = _menuTheme;
        _audio.Play();
    }

    private void Awake()
    {
        Observer.AddObservable(this);
    }

    private void OnDestroy()
    {
        Observer.RemoveObservable(this);
    }

    public void PlayerDied()
    {
        _audio.PlayOneShot(_death);
    }

    public void PlayerPassed(KittenType color)
    {
        _audio.PlayOneShot(_pass);
    }
}
