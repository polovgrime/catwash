﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IObservable
{
    void PlayerDied();
    void PlayerPassed(KittenType color);
}

public class Observer
{
    private static List<IObservable> observables = new List<IObservable>();
    public static void AddObservable(IObservable o)
    {
        observables.Add(o);
    }

    public static void RemoveObservable(IObservable o)
    {
        observables.Remove(o);
    }

    public static void PlayerDied()
    {
        foreach (var o in observables)
            o.PlayerDied();
    }

    public static void PlayerPassed(KittenType color)
    {
        foreach (var o in observables)
            o.PlayerPassed(color);
    }
}
