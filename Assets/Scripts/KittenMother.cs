﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.ComponentModel;
using System.Reflection;
using System;

public enum KittenType
{
    [Description("Blue")]
    Blue,
    [Description("White")]
    White,
    [Description("Grey")]
    Grey,
    Orange,
    [Description("Black")]
    Black,
    Green,
    Red,
    Brown
}
public static class KittenTypeEx
{
    public static string GetDescription<KittenType>(this KittenType enumerationValue)
    {
        var type = enumerationValue.GetType();
        var memberInfo = type.GetMember(enumerationValue.ToString());
        if (memberInfo != null && memberInfo.Length > 0)
        {
            var attrs = memberInfo[0].GetCustomAttributes(typeof(DescriptionAttribute), false);

            if (attrs != null && attrs.Length > 0)
            {
                //Pull out the description value
                return ((DescriptionAttribute)attrs[0]).Description;
            }
        }
        //If we have no description attribute, just return the ToString of the enum
        return enumerationValue.ToString();
    }
}
public class KittenMother : MonoBehaviour
{
    [SerializeField] private KittenType _kittenType;
    public bool CompareMother(KittenType type)
    {
        return type == _kittenType;
    }
}
