﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Kitten : MonoBehaviour
{
    [SerializeField] private KittenType _kittenType;
    public KittenType KittenType => _kittenType;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        var mother = collision.GetComponent<KittenMother>();
        if (mother != null)
        {
            if (mother.CompareMother(_kittenType))
            {
                Observer.PlayerPassed(_kittenType);
            }
            else
            {
                Observer.PlayerDied();
            }
            gameObject.SetActive(false);
        }      
    }
}
