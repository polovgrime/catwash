﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    private Animator _animator;
    private BoxCollider2D _boxCollider2D;
    protected Rigidbody2D _rigidbody2D;
    protected bool _isAgressive = false;
    private Vector2 _direction;
    [SerializeField] protected float _speed = 2f;
    [SerializeField] private int scoreToSpawn = 100;
    public void Awake()
    {
        if (Score.GetScore() < scoreToSpawn) gameObject.SetActive(false);
    }
    private void Start()
    {
        _direction = Random.Range(0, 10) > 10 ? Vector2.right : Vector2.left;
        SwitchDirection();
        _animator = GetComponent<Animator>();
        _boxCollider2D = GetComponent<BoxCollider2D>();
        _rigidbody2D = GetComponent<Rigidbody2D>();
    }

    protected virtual void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.GetComponent<Kitten>() != null)
            if (_isAgressive)
            {
                Observer.PlayerDied();
                collision.gameObject.SetActive(false);
            }
            else
            {
                _animator.SetTrigger("PlayerNear");
                _boxCollider2D.size = new Vector2(1, 1);
                _isAgressive = true;
            }
        else
        {
            SwitchDirection();
        }
    }

    protected virtual void FixedUpdate()
    {
        if (_isAgressive)
        {
            _rigidbody2D.MovePosition(_rigidbody2D.position + _direction * _speed * Time.fixedDeltaTime);
        }   
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        SwitchDirection();
    }

    private void SwitchDirection()
    {
        if (_direction == Vector2.right) 
        { 
            _direction = Vector2.left;
            transform.localScale = new Vector3(-1, 1);
        }
        else {
            _direction = Vector2.right;
            transform.localScale = new Vector3(1, 1);
        }
    }
}
