﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PlaneSpawner : MonoBehaviour
{
    [SerializeField] float _minimaleDistance = 1.5f;
    [SerializeField] List<KittenMother> _mothers;
    [SerializeField] Vector2 _leftBottomCorner = new Vector2(-10, -5);
    [SerializeField] Vector2 _rightTopCorner = new Vector2(10, 8);
    private List<Vector2> _freeSector = new List<Vector2>();
    private List<KittenMother> _inGameMothers = new List<KittenMother>();
    void Start()
    {
        for (var x = _leftBottomCorner.x; x < _rightTopCorner.x; x += _minimaleDistance)
        {
            for (var y = _leftBottomCorner.y; y < _rightTopCorner.y; y += _minimaleDistance)
            {
                var point = new Vector2(x, y);
                if (Vector2.Distance(Vector2.zero, point) > 10)
                {
                    _freeSector.Add(new Vector2(x, y));
                }
            }
        }
        foreach (var mom in _mothers)
        {
            _inGameMothers.Add(Instantiate(mom, GetRandomPosition(), Quaternion.identity));
        }
    }
    
    public Vector2 GetMotherPosition(KittenType type)
    {
        return _inGameMothers.First(e => e.CompareMother(type)).transform.position;
    }

    private Vector2 GetRandomPosition()
    {
        var position = _freeSector[Random.Range(0, _freeSector.Count)];
        _freeSector.Remove(position);
        return position;
    }
}
