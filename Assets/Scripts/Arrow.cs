﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arrow : MonoBehaviour
{
    [SerializeField] PlaneSpawner _spawner;
    [SerializeField] RandomKitten _kitten;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        var diff =  _spawner.GetMotherPosition(_kitten.CurrentKittenType) - (Vector2)_kitten.transform.position;
        diff.Normalize();

        var rot_z = Mathf.Atan2(diff.y, diff.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.Euler(0f, 0f, rot_z - 90);
    }
}
